public class TrafficLightPaul {

	public enum STATE {
		RED, ORANGE, GREEN
	};

	public static STATE lightState = STATE.GREEN;

	public int vehSpeed = 100;
	
	public TrafficLightPaul() {
		for (int i = vehSpeed; i > 0; i -= 5) {

			System.out.println("vechile speed is - " + i);
			System.out.println("Lights are " + lightState.toString());
			decision();

			// do nothing, sleep timer.
			if (i % 3 == 0) {
				lightState = STATE.ORANGE;
			} else if (i % 2 == 0) {
				lightState = STATE.RED;
			} else {
				lightState = STATE.GREEN;
			}
			vehSpeed = i;
		}
	}

	public void decision() {
		if (lightState == STATE.GREEN) {
			System.out.println("go through\n");
		} else if (lightState == STATE.ORANGE && vehSpeed < 40) {
			System.out.println("Slow down and stop if safe\n");
		} else if (lightState == STATE.ORANGE && vehSpeed > 40) {
			System.out.println("go through cautiously, next time slow down approaching a set of lights.\n");
		} else if (lightState == STATE.RED) {
			System.out.println("Stop.\n");
		}
	}

	public static void main(String[] args) {
		new TrafficLightPaul();
	}

}
