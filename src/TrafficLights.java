import java.util.Scanner;
public class TrafficLights {

	public void getStatesOfEverything() {

	}

	public enum STATE {
		RED, ORANGE, GREEN, FLASHING, OFF
	};

	public static STATE lightState = STATE.RED;

	public TrafficLights() {
//		int lightStatus = 0;
//		// 0 = Red
//		// 1 = Orange
//		// 2 = Green
//		// 3 = flashing / off

		boolean proceed = false;
		boolean pedestrianStat = false;
		boolean carTailGating = false;
		Scanner scanner = new Scanner(System.in);

		System.out.println("Is there a pedestrian on a crosswalk in the direction you are heading? True/False");
		String pedestrian = scanner.nextLine();
		int valid = this.checkValidInput(pedestrian);
		while (valid == -1) {
			System.out.println("Invalid answer. Please indicate only True or False");
			pedestrian = scanner.nextLine();
		}

		if (valid == 0) {
			pedestrianStat = true;
		} else {
			pedestrianStat = false;
		}

		System.out.println("Is there a car right behind you? True/False");
		String carsCloseBehind = scanner.nextLine();
		valid = this.checkValidInput(carsCloseBehind);
		while (valid == -1) {
			System.out.println("Invalid answer. Please indicate only True or False");
			carsCloseBehind = scanner.nextLine();
		}
		if (valid == 0) {
			carTailGating = true;
		} else {
			carTailGating = false;
		}

		System.out.println("What are the lights? Green/Orange/Red/Flashing/Off");
		String lightStatusInput = scanner.nextLine();

		while (!lightStatusInput.equalsIgnoreCase("Green") && !lightStatusInput.equalsIgnoreCase("Orange")
				&& !(lightStatusInput.equalsIgnoreCase("Red")) && !(lightStatusInput.equalsIgnoreCase("Flashing"))
				&& !(lightStatusInput.equalsIgnoreCase("Off"))) {
			System.out.println("Invalid answer. What are the lights? Green/Orange/Red/Flashing/Off");
			lightStatusInput = scanner.nextLine();
		}

		if (lightStatusInput.equalsIgnoreCase("Green")) {
//			lightStatus = 2;
			lightState = STATE.GREEN;
			if (pedestrianStat == false) {
				proceed = true;
			}
		} else if (lightStatusInput.equalsIgnoreCase("Orange")) {
//			lightStatus = 1;
			lightState = STATE.ORANGE;
			if (carTailGating == true && pedestrianStat == false) {
				proceed = true;
			}
		} else if (lightStatusInput.equalsIgnoreCase("Red")) {
//			lightStatus = 0;
			lightState = STATE.RED;
			proceed = false;
		} else if (lightStatusInput.equalsIgnoreCase("Flashing")) {
			lightState = STATE.FLASHING;
		} else if (lightStatusInput.equalsIgnoreCase("Off")) {
//			lightStatus = 3;
			lightState = STATE.OFF;
		}

		if (lightState == STATE.FLASHING || lightState == STATE.OFF) {
			
			System.out.println("Is there a car that you have to give way to? True/False");
			boolean needToGiveWay = scanner.nextBoolean();
			if (needToGiveWay == true) {
				proceed = false;
				System.out.println("Stop");
			} else {
				proceed = true;
				System.out.println("Please proceed through intersection");
			}
		} else {
			if ((lightState == STATE.RED || lightState == STATE.ORANGE) && proceed == false) {
				System.out.println("Stop");
			} else if ((lightState == STATE.GREEN || lightState == STATE.ORANGE) && proceed == true) {
				System.out.println("proceed through intersection");
			}
		}

	}

	public int checkValidInput(String userInput) {
		int statusResult = -1;
		switch ((userInput.toLowerCase().charAt(0))) {
		case 't':
			statusResult = 0;
			break;
		case 'y':
			statusResult = 0;
			break;
		case 'f':
			statusResult = 1;
			break;
		case 'n':
			statusResult = 1;
			break;
		case ' ':
			statusResult = -1;
			break;
		default:
			statusResult = -1;
			break;
		}
		return statusResult;
	}

	public String getLightStatus(String userInput) {
		String lightStatus = "";

		return lightStatus;
	}

	public static void main(String[] args) {
		TrafficLights trafLights = new TrafficLights();
		return;
	}
}
